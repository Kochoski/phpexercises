<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="style01.css">
</head>
<body>
<h1>Exercises</h1>
<br/>
<h2>1. Write a function to print your name, date of birth. and mobile number.</h2>
	<h3>Answer:</h3>
				
		<p class="red">
			<?php

				function person($name, $dateofbirth, $mobilenumber)
				{
					echo "Name: $name <br/> Date of birth: $dateofbirth <br/> Mobile Number: $mobilenumber";
								
				}

				person('Kostadin Kochoski', '05-04-1991','078-534-678');

			?>
		</p>
<hr/>
<h2>2. Write a function to compute the perimeter and area of a rectangle with a height of 7 inches. and width of 5 inches.</h2>
	<h3>Answer:</h3>
		
		<p class="yellow">
			<?php
				function perimeterArea($height, $width)
				{
					$perimeter = 2 * ($height + $width);
					$area = $height * $width;
					echo "The perimeter is $perimeter inc. <br/> The area is $area inc.";
				}
				perimeterArea('7', '5');
			?>
		</p>
<hr/>

<h2>3. Write a function to convert specified days into years, weeks and days.</h2>
	<h3>Answer:</h3>
		
		<p class="blue">
			<?php
				function convertdays($days)
				{
					
					$years = floor($days / 365);
					$leftdays = $days % 365;
					$weeks = floor($leftdays / 7);
					$leftdays = $weeks % 7; 
					$leapyear = floor($years / 4);
					$leftdays = ($weeks % 7) + $leapyear;
					echo "Years = $years <br/> Weeks = $weeks <br/> Days = $leftdays ";

				}
				// convertdays(600);
				convertdays(1001);
			?>
		</p>
<hr/>

<h2>4. Write a function that accepts three integers and find the maximum of them. </h2>
	<h3>Answer:</h3>
		
		<p class="pink">
			<?php
				function maximumInt($x, $y, $z)
				{
					if ($x > $y && $x > $z) 
					{
						echo "The maximum is $x. <br/>";
					}
					elseif ($y > $x && $y > $z) 
					{
						echo "The maximum is $y. <br/>";
					}
					elseif ($z > $x && $z > $y)
					{
						echo "The maximum is $z. <br/>";
					}
				}
				maximumInt(5,8,3);
				maximumInt(546561,54651363,6548);
			?>
		</p>
<hr/>
<h2>5. Write a function to calculate the distance between the two points.  </h2>
	<h3>Answer:</h3>
		
		<p class="violet">
			<?php
				function distance($a, $b)
				{
					$valueX = $b['x'] - $a['x'];
					$valueY = $b['y'] - $a['y']; 
					$distance = sqrt($valueX * $valueX + $valueY * $valueY);
					echo "The distance between A and B is $distance something.";
				}
			$a = [ 
				'x' => 5,
				'y' => 10];                                          
			$b = [ 
				'x' => 8,
				'y' => 15];
			distance($a,$b);
			?>
		</p>
<hr/>
<h2>6. Write a function that prints all even numbers between 1 and 50 (inclusive). </h2>
	<h3>Answer:</h3>
		
		<p class="magenta">
			<?php
				function evenNumbers($a, $b)
				{
					$even = [];
					for ($i=$a; $i <=$b ; $i++) 
					{ 
						if ($i % 2 == 0) 
						{
							array_push($even, $i);
						}
					}
					$evenprint = implode(', ', $even);
					return $evenprint;
				}

				$even1to50 = evenNumbers(1,50);
				echo "Even numbers between 1 and 50 are: $even1to50.";
			?>
		</p>
<hr/>
<h2> 7. Write a function program that converts Centigrade to Fahrenheit. </h2>
	<h3>Answer:</h3>
		
		<p class="lime">
			<?php
				function convertCelsiusToFahrenheit($celsius)
				{
					$result = $celsius * 1.8 +32;
					return $result;
				}
				$answer001 = convertCelsiusToFahrenheit(1);
				echo "Ex1: $answer001 F.<br/>";
				$answer002 = convertCelsiusToFahrenheit(50);
				echo "Ex2: $answer002 F.<br/>";
			?>
		</p>
<hr/>
<h2>8. Write a function that takes hours and minutes as input, and calculates the total number of minutes. </h2>
	<h3>Answer:</h3>
		
		<p class="chocolate">
			<?php
				function calculatingMins($hours,$minutes)
				{
					$allminutes = $hours * 60 + $minutes;
					echo "$allminutes mins.";
				}
				calculatingMins(3,9);
			?>
		</p>
<hr/>
<h2>9. Write a function that converts kilometers per hour to miles per hour. </h2>
	<h3>Answer:</h3>
		
		<p class="blue">
			<?php
				function convertKmToMiles($km)
				{
					$mph = $km * 0.621371;
					echo "$mph mph.";
				}
				convertKmToMiles(10);
			?>
		</p>
<hr/>
<h2> 10. Write a function to check whether a given number is positive or negative. </h2>
	<h3>Answer:</h3>
		
		<p class="magenta">
			<?php
				function negativeOrPositive($number)
				{
					if ($number < 0) 
					{
						echo "The $number is negative number.<br/>";
					}
					elseif ($number > 0)
					{
						echo "The $number is positive number. <br/>";
					}
					else
					{
						echo "FATAL...<br/>";
					}
				}
				negativeOrPositive(0);
				negativeOrPositive(50);
				negativeOrPositive(-45);	
			?>
		</p>
<hr/>
<h2> 11.Write a function to read the age of a candidate and determine whether it is eligible for casting his/her own vote </h2>
	<h3>Answer:</h3>
		
		<p class="violet">
			<?php
				function eligible($age)
				{
					if ($age > 18) 
					{
						echo "The candidate is eligible. <br/>";
					}
					else
					{
						echo "FATAL... You need to grow up. <br/>";
					}
				}
				eligible(25);
				eligible(16);
			?>
		</p>
<hr/>
<h2> 12.Write a function to check whether a character is an alphabet, digit or special character. </h2>
	<h3>Answer:</h3>
		
		<p class="yellow">
			<?php
				function check($character)
				{
					if (ctype_alpha($character) ) 
					{
						echo "The `$character` is alphabet. <br/>";
					}
					elseif (ctype_digit($character)) 
					{
						echo "The `$character` is digit. <br/>";
					}
					else
					{
						echo "The `$character` is special character. <br/>";
					}
				}
				check('A');
				check('5');
				check('@');	
			?>
		</p>
<hr/>
<h2>13. Write a function to receive an array of numbers and print their names with text. ex. 4 => 'Four' </h2>
	<h3 class="answer13">Answer:</h3>
		
		<p class="lime">
			<?php
				function numberstotext($array)
				{
					$text = [];
					foreach ($array as $key => $value) {
						$num = floor($value / 10);
						if ($num == 0) {
							if ($value == 0) {
								array_push($text, "zero");
							}elseif ($value == 1) {
								array_push($text, "one");
							}elseif ($value == 2) {
								array_push($text, "two");
							}elseif ($value == 3) {
								array_push($text, "tree");
							}elseif ($value == 4) {
								array_push($text, "four");
							}elseif ($value == 5) {
								array_push($text, "five");
							}elseif ($value == 6) {
								array_push($text, "six");
							}elseif ($value == 7) {
								array_push($text, "seven");
							}elseif ($value == 8) {
								array_push($text, "eight");
							}elseif ($value == 9) {
								array_push($text, "nine");
							}
							
						}
					}
					
					$final = array_combine($array, $text);
					return $final;
				}
				print_r(numberstotext([6,1,2,9,4,5]));
			?>
		</p>
<hr/>
<h2>14. Write a function to receive any Month Number in integer and display Month name in the word. 3 => March  </h2>
	<h3>Answer:</h3>
		
		<p class="red">
			<?php
				function nameofmonth($a)
				{
					if ($a==1) {
						echo "The first month of the year is January.<br/>";
					}elseif ($a==2) {
						echo "The second month of the year is February.<br/>";
					}elseif ($a==3) {
						echo "The third month of the year is March.<br/>";
					}elseif ($a==4) {
						echo "The fourth month of the year is April.<br/>";
					}elseif ($a==5) {
						echo "The fifth month of the year is May.<br/>";
					}elseif ($a==6) {
						echo "The sixth month of the year is June.<br/>";
					}elseif ($a==7) {
						echo "The seventh month of the year is July.<br/>";
					}elseif ($a==8) {
						echo "The eighth month of the year is August.<br/>";
					}elseif ($a==9) {
						echo "The ninth month of the year is September.<br/>";
					}elseif ($a==10) {
						echo "The tenth month of the year is October.<br/>";
					}elseif ($a==11) {
						echo "The eleventh month of the year is November.<br/>";
					}elseif ($a==12) {
						echo "The twelfth month of the year is December.<br/>";
					}else
					{
						echo "FATAL... Go back to school and learn the months of the year.<br/>";
					}
				}

				
				nameofmonth(5);
				nameofmonth(13);
			?>
		</p>

</body>
</html>
